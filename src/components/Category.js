import React from 'react'

export default function Category(props){
  return (
      <a className="nav-link active" aria-current="page" href={props.id}>{props.title}</a>
  )
}
