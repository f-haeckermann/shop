import React from 'react';
import Flag_of_Germany from '../images/Flag_of_Germany.svg';
import Flag_of_the_United_Kingdom from '../images/Flag_of_the_United_Kingdom.svg';
import Request from './request/Request';
import { UserContext } from './../UserContext';
import { useTranslation } from 'react-i18next';

export default function Navbar() {
  const [modalShow, setModalShow] = React.useState(false);
  const { user } = React.useContext(UserContext);
  const { t, i18n } = useTranslation();

  function TranslateClick(lang) {
    i18n.changeLanguage(lang);
  }

  return (
    <div className="col-12">
      <div className="row">
        <nav className="navbar navbar-light bg-light shadow">
          <div className="w-100">
            <div className="container">
              <div className="d-flex flex-column flex-lg-row align-items-stretch justify-content-start">
                <a className="navbar-brand" href="./">
                  3D Digital Warehouse
                </a>
                <ul className="navbar-nav flex-row d-flex flex-1 w-100 ">
                  <li className="nav-item">
                    <a
                      className="nav-link active p-2"
                      aria-current="page"
                      href="/about"
                    >
                      {t('Pages.About')}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link p-2" href="/services">
                      {t('Pages.Services')}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link p-2"
                      href="/contact"
                      tabIndex="-1"
                      aria-disabled="true"
                    >
                      {t('Pages.Contact')}
                    </a>
                  </li>
                  <li className="nav-item ms-auto">
                    <button
                      onClick={() => setModalShow(true)}
                      className="p-2 btn btn-warning btn-sm ms-4"
                    >
                      REQUEST PART
                    </button>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link ms-3 p-2"
                      href="/contact"
                      tabIndex="-1"
                      aria-disabled="true"
                    >
                      Signed as: {user.firstname} {user.lastname}
                    </a>
                  </li>
                  <li className="nav-item">
                    <div className="p-2 ms-4">
                      {i18n.language === 'en' ? (
                        <img
                          onClick={() => TranslateClick('de')}
                          width="30px"
                          className="pointer"
                          src={Flag_of_Germany}
                          alt="Switch to German"
                        />
                      ) : (
                        <img
                          onClick={() => TranslateClick('en')}
                          width="30px"
                          className="pointer"
                          src={Flag_of_the_United_Kingdom}
                          alt="Switch to English"
                        />
                      )}
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
      </div>
      <div className="request--Main">
        <Request setModalShow={setModalShow} modalShow={modalShow} />
      </div>
    </div>
  );
}
