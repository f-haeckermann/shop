import React from 'react';
import CartItem from './CartItem';
import { ShoppingCart, ChevronDown } from 'react-feather';
import NumberFormat from 'react-number-format';
import { useTranslation } from 'react-i18next';

export default function CartBox(props) {
  const { t } = useTranslation();

  const { itemsOnCart, handleAddItem, handleRemoveItem } = props;
  const [isOpen, setIsOpen] = React.useState(false);

  const cartElements = itemsOnCart.map((item) => (
    <CartItem
      product={item}
      addItem={handleAddItem}
      removeItem={handleRemoveItem}
      key={item.id}
    />
  ));
  function onToggleCart() {
    setIsOpen((prevIsOpen) => !prevIsOpen);
  }
  const cartSum = itemsOnCart.reduce(
    (a, c) => a + parseFloat(c.price) * c.quantity,
    0
  );
  const cartItems = itemsOnCart.reduce((a, c) => a + c.quantity, 0);

  return (
    <div
      className={`Cart ${
        itemsOnCart.length > 0 && 'open'
      } text-white d-flex flex-column align-items-around`}
    >
      <div className="d-flex align-items-center w-100 justify-content-center sticky-top first">
        <ShoppingCart size="17px" color="white" />
        <span className="mx-2">{t('Cart.Title')}</span>
        <span className="badge bg-secondary">{cartItems}</span>
        {itemsOnCart.length > 0 && (
          <ChevronDown
            onClick={onToggleCart}
            className="ms-2 pointer me-auto"
            size="17px"
            color="white"
          />
        )}
        {itemsOnCart.length > 0 && (
          <button className="btn btn-warning ms-4">{t('Cart.PayNow')}</button>
        )}
      </div>
      {isOpen && itemsOnCart.length > 0 && (
        <ul className="mt-2 list-group list-group-flush">
          <li className="list-group-item bg-transparent text-white d-flex px-0 align-items-center">
            <div className="cart--Image">&nbsp;</div>
            <div className="ms-3 me-auto cart--Name text-start">
              {t('Cart.ArticleName')}
            </div>
            <div className="ms-3 cart--Quantity text-center">
              {t('Cart.ArticleQuantity')}
            </div>
            <div className="ms-3 cart--Price text-end">
              {t('Cart.ArticlePrice')}
            </div>
          </li>
          {cartElements}
        </ul>
      )}
      {isOpen && itemsOnCart.length > 0 && (
        <div className="d-flex align-items-center justify-content-center sticky-top last">
          <span className="mx-2">{t('Cart.Subtotal')}</span>
          <span className="">
            <NumberFormat
              thousandsGroupStyle="thousand"
              value={cartSum}
              decimalSeparator="."
              displayType={'text'}
              thousandSeparator={true}
              allowNegative={true}
              fixedDecimalScale={true}
              decimalScale={2}
              suffix={'€'}
            />
          </span>
          {itemsOnCart.length > 0 && (
            <button className="btn btn-warning ms-auto">
              {t('Cart.PayNow')}
            </button>
          )}
        </div>
      )}
    </div>
  );
}
