import React from "react";
import NumberFormat from 'react-number-format';
import {PlusCircle, MinusCircle} from 'react-feather'

export default function CartItem (props){
    const {addItem, removeItem, product} = props

    return (
        <li className="list-group-item bg-transparent text-white d-flex px-0 align-items-center">
            <div className="cart--Image">
                <img alt={product.name} className="shadow" width="100%" src={product.images[0].src} />
            </div>
            <div className="ms-3 me-auto cart--Name text-start text-truncate">
                {product.name}
            </div>
            <div className=" cart--Quantity text-center d-flex align-items-center justify-content-center">
                <button className="bg-transparent p-0 border-0 h6 m-0" onClick={()=>removeItem(product)}><MinusCircle size="14px" color="white"/></button>
                <div className="bg-transparent p-0 border-0 mx-1">{product.quantity}</div>
                <button className="bg-transparent p-0 border-0 h6 m-0" onClick={()=>addItem(product)}><PlusCircle size="14px" color="white"/></button>
            </div>
            <div className="ms-3 cart--Price text-end">
                <NumberFormat
                    thousandsGroupStyle="thousand"
                    value={product.price}
                    decimalSeparator="."
                    displayType={"text"}
                    thousandSeparator={true}
                    allowNegative={true}
                    fixedDecimalScale={true}
                    decimalScale={2}
                    suffix={'€'}
                />
            </div>
        </li>
    )
}