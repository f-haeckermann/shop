import React from 'react'
import CategoriesData from '../../CategoriesData'
import Category from "../Category";

export default function ContainerCategories () {
  const categoriesElements = CategoriesData.map(category => {
    return <Category  key={category.id} {...category}/>
  })

  return (
        <div className="categories--Container d-flex flex-row flex-md-column xnav">
          {categoriesElements}
        </div>
  )
}
