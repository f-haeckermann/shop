import React from 'react';
import { Minus, Plus } from 'react-feather';
import { useTranslation } from 'react-i18next';

export default function ButtonArea(props) {
  const { t } = useTranslation();

  const { product, addItem, removeItem, itemsOnCart } = props;
  const item =
    itemsOnCart.length > 0 &&
    itemsOnCart.find((elem) => elem.id === product.id);

  return item ? (
    <div className="product--Button-area">
      <div className="product--Button-container d-flex justify-content-around mt-3">
        <button
          title="Remove item"
          onClick={() => removeItem(product)}
          className="product--Button-add btn btn-sm  btn-light"
        >
          <Minus size="13" />{' '}
        </button>

        <button
          onClick={() => addItem(product)}
          className="product--Button btn btn-block btn-warning position-relative"
        >
          {t('Product.AddItem')}
          <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
            {item.quantity}
            <span className="visually-hidden">unread messages</span>
          </span>
        </button>
        <button
          title="Add item"
          onClick={() => addItem(product)}
          className="product--Button-remove btn btn-sm btn-light"
        >
          <Plus size="13" />
        </button>
      </div>
    </div>
  ) : (
    <div className="product--Button-area">
      <div className="product--Button-container d-flex justify-content-around mt-3">
        <button
          onClick={() => addItem(product)}
          className="product--Button btn btn-warning"
        >
          {t('Product.AddToCart')}
        </button>
      </div>
    </div>
  );
}
