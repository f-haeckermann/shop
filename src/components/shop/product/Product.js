import React from 'react';
import { Star, Check } from 'react-feather';
import ButtonArea from './ButtonArea';
import NumberFormat from 'react-number-format';
import { useTranslation } from 'react-i18next';

export default function Product(props) {
  const { t } = useTranslation();

  const { product, addItem, itemsOnCart, removeItem } = props;
  const item =
    itemsOnCart.length > 0 &&
    itemsOnCart.find((elem) => elem.id === product.id);
  const [isFavorite, setIsFavorite] = React.useState(false);

  function toggleFavorite() {
    setIsFavorite((prevIsFavorite) => !prevIsFavorite);
  }

  return (
    <div className="product--Container  col-12 col-sm-6 col-lg-4 mb-4">
      <div className="text-center position-relative">
        {product.on_sale && (
          <span className="product--Offer shadow">
            {t('Product.SpecialOffer')}
          </span>
        )}
        {item && (
          <span className="product--Button-message text-success shadow">
            <Check size="13" /> {t('Product.AddedToCart')}
          </span>
        )}
        <img
          alt={product.name}
          className="product--Image shadow"
          width="100%"
          src={product.images[0].src}
        />
        <span onClick={toggleFavorite} className="product--Favorite pointer">
          {isFavorite ? (
            <Star fill="orange" size="20" color="orange" />
          ) : (
            <Star fill="none" size="20" color="orange" />
          )}
        </span>
      </div>
      <h2 className="product--Title h6 mt-3 text-truncate">{product.name}</h2>
      <p className="product--Price h6 fw-bold">
        <NumberFormat
          thousandsGroupStyle="thousand"
          value={product.price}
          decimalSeparator="."
          displayType={'text'}
          thousandSeparator={true}
          allowNegative={true}
          fixedDecimalScale={true}
          decimalScale={2}
          suffix={'€'}
        />
      </p>
      <p className="product--ShortDescription text-truncate">
        {product.mini_desc}
      </p>
      <ButtonArea
        itemsOnCart={itemsOnCart}
        product={product}
        addItem={addItem}
        removeItem={removeItem}
      />
    </div>
  );
}
