import { useState, useEffect } from 'react';
import Product from './product/Product';
import { useTranslation } from 'react-i18next';

export default function ListProducts(props) {
  const { handleAddItem, handleRemoveItem, itemsOnCart } = props;
  //Data Fetch
  const [products, setProducts] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const { i18n } = useTranslation();
  const language = i18n.language;
  useEffect(() => {
    fetch(
      `https://oldtimerparts.de/wp-json/wc/v2/products?per_page=20&lang=${language}&category=439&consumer_key=ck_156f0b18377d53581cb1b9848a5bb76be7832c13&consumer_secret=cs_8cdbecda3e286e2a069496fed1159abd94882c96`
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `This is an HTTP error: The status is ${response.status}`
          );
        }
        return response.json();
      })
      .then((actualData) => {
        setProducts(actualData);
        setError(null);
      })
      .catch((err) => {
        setError(err.message);
        setProducts(null);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [language]);

  return (
    <div className="products--Container row justify-content-lg-start justify-content-center">
      {loading && <div>A moment please...</div>}
      {error && (
        <div>{`There is a problem fetching the post data - ${error}`}</div>
      )}
      {products &&
        products.map((product) => {
          return (
            <Product
              itemsOnCart={itemsOnCart}
              key={product.id}
              product={product}
              addItem={handleAddItem}
              removeItem={handleRemoveItem}
            />
          );
        })}
    </div>
  );
}
