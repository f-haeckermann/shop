import React from 'react';
import Form from 'react-bootstrap/Form';
import FloatingLabel from 'react-bootstrap/FloatingLabel';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useTranslation } from 'react-i18next';

export default function FormContent() {
  const { t } = useTranslation();

  const [formData, setFormData] = React.useState({
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    quantity: 1,
    width: 0,
    large: 0,
    height: 0,
    data: false,
    marketing: false,
    material: '',
  });

  const maxQuantity = 100;
  const listOptionsQuantity = [...Array(maxQuantity)].map((el, index) => (
    <option value={parseFloat(index + 1)} key={index}>
      {index + 1}
    </option>
  ));

  function handleChangeEvent(event) {
    const { name, value, type, checked } = event.target;

    setFormData((prevState) => {
      return {
        ...prevState,
        [name]: type === 'checkbox' ? checked : value,
      };
    });
  }

  function handleSubmit(event) {
    event.preventDefault();
    console.log(formData);
  }

  console.log(formData);
  return (
    <form onSubmit={handleSubmit}>
      <div className="row">
        <div className="col-12">
          <p className="fs-6">{t('Form.Material')}</p>
        </div>
      </div>
      {['radio'].map((type) => (
        <div key={`inline-${type}`} className="mb-3">
          <Form.Check
            inline
            label={t('Form.Plastic')}
            name="material"
            onChange={handleChangeEvent}
            value="plastic"
            checked={formData.material === 'plastic'}
            type={type}
          />
          <Form.Check
            inline
            label={t('Form.Metal')}
            name="material"
            value="metal"
            onChange={handleChangeEvent}
            checked={formData.material === 'metal'}
            type={type}
          />
          <Form.Check
            inline
            label={t('Form.Rubber')}
            name="material"
            value="gummi"
            onChange={handleChangeEvent}
            checked={formData.material === 'gummi'}
            type={type}
          />
          <Form.Check
            inline
            label={t('Form.Digitalization')}
            name="material"
            value="digital"
            checked={formData.material === 'digital'}
            onChange={handleChangeEvent}
            type={type}
          />
        </div>
      ))}
      <div className="row g-2">
        <div className="col-3">
          <FloatingLabel controlId="floatingSelect" label={t('Form.Quantity')}>
            <Form.Select
              name="quantity"
              onChange={handleChangeEvent}
              value={parseFloat(formData.quantity)}
              aria-label="Floating label select example"
            >
              {listOptionsQuantity}
            </Form.Select>
          </FloatingLabel>
        </div>
        <div className="col-9">
          <div className="row g-2">
            <div className="col-4">
              <div className="row">
                <div className="col-12">
                  <FloatingLabel
                    controlId="floatingInput"
                    label={t('Form.Width')}
                    className="mb-3"
                    name="width"
                    onChange={handleChangeEvent}
                    value={formData.width}
                  >
                    <Form.Control
                      onChange={handleChangeEvent}
                      name="width"
                      value={formData.width}
                      type="text"
                      placeholder={t('Form.Width')}
                    />
                  </FloatingLabel>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <Form.Range
                    onChange={handleChangeEvent}
                    name="width"
                    value={formData.width}
                  />
                </div>
              </div>
            </div>
            <div className="col-4">
              <div className="row">
                <div className="col-12">
                  <FloatingLabel
                    controlId="floatingInput"
                    label={t('Form.Large')}
                    className="mb-3"
                    name="large"
                    onChange={handleChangeEvent}
                    value={formData.large}
                  >
                    <Form.Control
                      onChange={handleChangeEvent}
                      value={formData.large}
                      name="large"
                      type="text"
                      placeholder={t('Form.Large')}
                    />
                  </FloatingLabel>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <Form.Range
                    onChange={handleChangeEvent}
                    name="large"
                    value={formData.large}
                  />
                </div>
              </div>
            </div>
            <div className="col-4">
              <div className="row">
                <div className="col-12">
                  <FloatingLabel
                    controlId="floatingInput"
                    label={t('Form.Height')}
                    className="mb-3"
                    name="height"
                    onChange={handleChangeEvent}
                    value={formData.height}
                  >
                    <Form.Control
                      onChange={handleChangeEvent}
                      name="height"
                      value={formData.height}
                      type="text"
                      placeholder={t('Form.Height')}
                    />
                  </FloatingLabel>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <Form.Range
                    onChange={handleChangeEvent}
                    name="height"
                    value={formData.height}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Row>
        <Col>
          <FloatingLabel
            controlId="floatingInput"
            label={t('Form.FirstName')}
            className="mb-3"
            onChange={handleChangeEvent}
            value={formData.firstName}
          >
            <Form.Control
              onChange={handleChangeEvent}
              name="firstName"
              type="text"
              value={formData.firstName}
              placeholder={t('Form.FirstName')}
            />
          </FloatingLabel>
        </Col>
        <Col>
          <FloatingLabel
            controlId="floatingPassword"
            label={t('Form.LastName')}
            onChange={handleChangeEvent}
            value={formData.lastName}
          >
            <Form.Control
              name="lastName"
              onChange={handleChangeEvent}
              type="text"
              value={formData.lastName}
              placeholder={t('Form.LastName')}
            />
          </FloatingLabel>
        </Col>
      </Row>
      <Row>
        <Col>
          <FloatingLabel
            controlId="floatingInput"
            label={t('Form.Email')}
            className="mb-3"
            onChange={handleChangeEvent}
            value={formData.email}
          >
            <Form.Control
              onChange={handleChangeEvent}
              name="email"
              type="text"
              value={formData.email}
              placeholder={t('Form.Email')}
            />
          </FloatingLabel>
        </Col>
        <Col>
          <FloatingLabel
            controlId="floatingPassword"
            label={t('Form.Phone')}
            onChange={handleChangeEvent}
            value={formData.phone}
          >
            <Form.Control
              name="phone"
              type="phone"
              value={formData.phone}
              onChange={handleChangeEvent}
              placeholder={t('Form.Phone')}
            />
          </FloatingLabel>
        </Col>
      </Row>
      <Row>
        <Col>
          {['checkbox'].map((type) => (
            <div key={`default-${type}`} className="mb-0">
              <Form.Check
                className="small"
                type={type}
                name="data"
                onChange={handleChangeEvent}
                value={formData.data}
                checked={formData.data}
                label={t('Form.AGB')}
              />

              <Form.Check
                className="small"
                type={type}
                name="marketing"
                onChange={handleChangeEvent}
                value={formData.marketing}
                checked={formData.marketing}
                label={t('Form.Marketing')}
              />
            </div>
          ))}
        </Col>
      </Row>
    </form>
  );
}
