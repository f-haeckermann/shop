import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Form from './FormContent';
import { useTranslation } from 'react-i18next';

export default function Request(props) {
  const { setModalShow, modalShow } = props;
  const { t } = useTranslation();

  function MyVerticallyCenteredModal(props) {
    return (
      <Modal
        {...props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            {t('Form.Title')}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form />
        </Modal.Body>
        {/* <Modal.Footer>

                    <button onClick={props.onHide}>Next</button>

                </Modal.Footer>*/}
      </Modal>
    );
  }
  return (
    <div>
      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </div>
  );
}
