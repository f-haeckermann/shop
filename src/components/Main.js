import React from 'react'
import ListProducts from './shop/ListProducts'
import ContainerCategories from "./shop/ListCategories";
import CartBox from "./shop/cart/CartBox";

export default function Main() {
  const [itemsOnCart, setItemsOnCart] = React.useState(
      ()=> JSON.parse(localStorage.getItem("items")) || []
  )
  //Set Items on Local Storage
  React.useEffect(()=>{
    localStorage.setItem("items", JSON.stringify(itemsOnCart))
  },[itemsOnCart])

  const addItem = (product) => {
    const exist = itemsOnCart.find((x) => x.id === product.id)
    if (exist) {
      setItemsOnCart(prevState => prevState.map(value => value.id === product.id ? {...exist, quantity: exist.quantity +1 }: value))
    } else{
      setItemsOnCart(prevState => [...prevState, {...product, quantity: 1}])
    }
  }

  const removeItem = (product) => {
    const exist = itemsOnCart.find((x) => x.id === product.id)
    if (exist.quantity === 1) {
      setItemsOnCart(prevState => prevState.filter(value => value.id !== product.id ))
    } else {
      setItemsOnCart(prevState => prevState.map(value => value.id === product.id ? {...exist, quantity: exist.quantity - 1 }: value))
    }
  }

  return(
      <div className="col-12">
        <CartBox handleAddItem={addItem} handleRemoveItem={removeItem} itemsOnCart={itemsOnCart} />
        <div className="row">
          <nav className="bg-white">
            <div className="w-100">
              <div className="container">
                <section className="row">
                  <div className="col-12 col-md-4 col-lg-3 position-sticky h-100 categories--List" >
                    <aside  className="bg-light h-100 my-4  shadow py-2">
                      <nav className="nav flex-row flex-md-column nav-fab">
                        <ContainerCategories  />
                      </nav>
                    </aside>
                  </div>
                  <div className="col-12 col-md-8 col-lg-9 my-lg-4">
                    <ListProducts itemsOnCart={itemsOnCart} handleAddItem={addItem} handleRemoveItem={removeItem} />
                  </div>
                </section>
              </div>
            </div>
          </nav>
        </div>
      </div>
  )
}
