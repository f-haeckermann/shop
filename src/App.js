import React from 'react';
import Navigation from './components/Navigation';
import Main from './components/Main';
import { UserContext } from './UserContext';

export default function App() {
  const user = {
    firstname: 'Fabian',
    lastname: 'Haeckermann',
    email: 'fabianhaeckermann@gmail.com',
    phone: '+491785117708',
  };
  return (
    <UserContext.Provider value={{ user }}>
      <div className="container-fluid">
        <div className="row sticky-top">
          <Navigation />
        </div>
        <div className="row">
          <Main />
        </div>
      </div>
    </UserContext.Provider>
  );
}
