const data = {
    products: [
        {
            id: 1,
            title: "First Product ",
            price: "19.20",
            rating: 5,
            model: ["my200", "fz500", "nh120"],
            offer: true,
            description: "This is the short description for the product number 1",
            quantity: 0
        }, {
            id: 2,
            title: "Second Product",
            price: "99.20",
            rating: 4,
            model: ["my200", "fz500"],
            offer: false,
            description: "This is the short description for the product number 2",
            quantity: 0
        }, {
            id: 3,
            title: "Third Product",
            price: "12.2",
            rating: 3,
            model: ["my200", "fz500", "nh120"],
            offer: true,
            description: "This is the short description for the product number 3",
            quantity: 0

        }, {
            id: 4,
            title: "Fourth Product",
            price: "13.15",
            rating: 3,
            model: ["my200", "fz500", "nh120"],
            offer: false,
            description: "This is the short description for the product number 3",
            quantity: 0

        }, {
            id: 5,
            title: "Fifth Product",
            price: "24.19",
            rating: 3,
            model: ["my200", "fz500", "nh120"],
            offer: true,
            description: "This is the short description for the product number 3",
            quantity: 0

        }, {
            id: 6,
            title: "Sixth Product ",
            price: "24.87",
            rating: 3,
            model: ["my200", "fz500", "nh120"],
            offer: true,
            description: "This is the short description for the product number 3",
            quantity: 0
        }, {
            id: 7,
            title: "Seventh Product",
            price: "24.3",
            rating: 3,
            model: ["my200", "fz500", "nh120"],
            offer: false,
            description: "This is the short description for the product number 3",
            quantity: 0
        }, {
            id: 8,
            title: "Eight Product",
            price: "23.6",
            rating: 3,
            model: ["my200", "fz500", "nh120"],
            offer: true,
            description: "This is the short description for the product number 3",
            quantity: 0
        }
    ]
}

export default data
