const data = [
  {
    id: 1,
    title: 'Audi',
    description: 'This is the short description for the product number 1',
  },
  {
    id: 2,
    title: 'BMW',
    description: 'This is the short description for the product number 2',
  },
  {
    id: 3,
    title: 'Volkswagen',
    description: 'This is the short description for the product number 3',
  },
  {
    id: 5,
    title: 'Peugeot',
    description: 'This is the short description for the product number 3',
  },
  {
    id: 6,
    title: 'Ford',
    description: 'This is the short description for the product number 3',
  },
  {
    id: 7,
    title: 'Tesla',
    description: 'This is the short description for the product number 3',
  },
  {
    id: 8,
    title: 'Porsche',
    description: 'This is the short description for the product number 3',
  },
  {
    id: 9,
    title: 'Ferrari',
    description: 'This is the short description for the product number 3',
  },
];

export default data;
