import React, { Suspense } from 'react';
import ReactDom from 'react-dom';
import './styles.css';
import App from './App';
import './multilanguage';

ReactDom.render(
  <Suspense fallback="loading">
    <App />
  </Suspense>,
  document.getElementById('root')
);
