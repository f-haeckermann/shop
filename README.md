# SHOP by Fabian Haeckermann (YEAR 2022)#

This is a Basic Shop created to train ReactJS skills

### Tech ? ###

* ReactJS
* CSS
* HTML

### How do I get set up? ###

- git clone
- get in the folder shop 
- npm start


### Questions ? ###

fabianhaeckermann@gmail.com